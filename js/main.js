"use strict";

const crossEyes = Array.from(document.getElementsByClassName("fa-eye-slash"));

crossEyes.forEach((crossEye) => {
  crossEye.style.display = "none";
});

const eyes = Array.from(document.getElementsByClassName("fa-eye"));
const inputPass = Array.from(document.querySelectorAll(".password-form input"));

onClick(eyes, inputPass, crossEyes, "text");
onClick(crossEyes, inputPass, eyes, "password");

function onClick(arrToEl, arrToChange, arrToBlock, typeToChange) {
  arrToEl.forEach((el) => {
    el.addEventListener("click", function () {
      el.style.display = "none";
      arrToChange[`${arrToEl.indexOf(el)}`].type = typeToChange;
      arrToBlock[`${arrToEl.indexOf(el)}`].style.display = "block";
    });
  });
}

// eyes.forEach((eye) => {
//   eye.addEventListener("click", function () {
//     eye.style.display = "none";
//     inputPass[`${eyes.indexOf(eye)}`].type = "text";
//     crossEyes[`${eyes.indexOf(eye)}`].style.display = "block";
//   });
// });

// crossEyes.forEach((crossEye) => {
//   crossEye.addEventListener("click", function () {
//     crossEye.style.display = "none";
//     inputPass[`${crossEyes.indexOf(crossEye)}`].type = "password";
//     eyes[`${crossEyes.indexOf(crossEye)}`].style.display = "block";
//   });
// });

const btn = document.getElementsByClassName("btn")[0];

btn.addEventListener("click", function () {
  // To remove existed elements
  if (document.getElementById("inputError") !== null) {
    inputError.remove();
  }

  if (inputPass[0].value.trim() === "" || inputPass[1].value.trim() === "") {
    const inputError = createInputError("Нужно ввести значения");
    btn.before(inputError);
  } else if (inputPass[0].value !== inputPass[1].value) {
    const inputError = createInputError("Нужно ввести одинаковые значения");
    btn.before(inputError);
  } else {
    if (document.getElementById("inputError") !== null) {
      inputError.remove();
    }
    alert("You are welcome");
  }
});

function createInputError(textContent) {
  const inputError = document.createElement("div");
  inputError.id = "inputError";
  inputError.textContent = textContent;
  inputError.style.color = "red";
  return inputError;
}
